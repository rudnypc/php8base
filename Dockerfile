FROM alpine:3.13 AS iconv-build
RUN apk add --no-cache gnu-libiconv

FROM php:8.1.19-fpm-alpine
COPY --from=iconv-build /usr/lib/preloadable_libiconv.so /usr/lib/preloadable_libiconv.so
LABEL Maintainer="Tim de Pater <code@trafex.nl>"
LABEL Description="Lightweight container with Nginx 1.20 & PHP 8.0 based on Alpine Linux."

RUN apk add --no-cache gnu-libiconv

RUN  apk --no-cache add \
  curl \
  nginx \
  php \
  supervisor
RUN apk add --no-cache file imagemagick
RUN apk add php-simplexml php-xml php-xmlreader php-zip --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/
RUN apk add --update libcurl curl-dev libpng-dev icu-dev build-base \
        gcc \
        g++ \
        autoconf \
        tar \
		  libxslt-dev \
        tidyhtml-dev \
        net-snmp-dev \
        aspell-dev \
        freetds-dev \
        openldap-dev \
        gettext-dev \
        imap-dev \
        openssh \
        sudo \
        make \
        shadow \
        libmcrypt-dev \
        gmp-dev \
        openssl \
        mariadb-client \
        curl \
        freetype \
        libpng \
        libjpeg-turbo \
        freetype-dev \
        libpng-dev \
        libjpeg-turbo-dev \
        libwebp-dev \
        libzip-dev \
        bzip2-dev \
        postgresql-dev \
        supervisor \
        bash \
        nginx \
        pngquant \
        jpegoptim \
        zip \
        icu-dev \
        libxml2-dev \
        dcron \
        wget \
        rsync \
        ca-certificates \
        oniguruma-dev
RUN docker-php-ext-configure gd \
    --with-webp \
    --with-jpeg \
    --with-freetype
	
RUN   docker-php-ext-install intl
RUN   docker-php-ext-install mysqli 
#RUN   docker-php-ext-install opcache 
RUN   docker-php-ext-install gd 
RUN   docker-php-ext-install zip 
  
  
 RUN   docker-php-ext-enable \
   intl \
  mysqli \
  #opcache \
  zip 

#ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php
#RUN apk add musl libcrypto3 libssl3 --repository http://dl-3.alpinelinux.org/alpine/edge/main/ --allow-untrusted && apk add songrec --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community/

# Cleanup
RUN  rm -rf /var/cache/apk/*

# Create symlink so programs depending on `php` still function
#RUN ln -s /usr/bin/php8 /usr/bin/php

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php81/php-fpm.d/www.conf
COPY config/php.ini /usr/local/etc/php/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody.nobody /var/www/html && \
  chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/nginx
ENV ARCH=arm64

#RUN curl -O https://downloads.rclone.org/rclone-current-linux-${ARCH}.zip && \
#    unzip rclone-current-linux-${ARCH}.zip && \
#    cp rclone-*-linux-${ARCH}/rclone /usr/bin/ && \
#    chown root:root /usr/bin/rclone && \
#    chmod 755 /usr/bin/rclone

# Switch to use a non-root user from here on
#USER nobody

# Add application
WORKDIR /var/www/html
COPY --chown=nobody src/ /var/www/html/
#COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
#RUN composer require rakibtg/sleekdb

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
#HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping
